//
// Created by csreed on 10/31/19.
//

#ifndef FILE_LIB_H
#define FILE_LIB_H

#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

/**
 * @brief Opens the file whose name is the string pointed to by pathname and associates a stream with it.
 * @param pathname points to the file to be opened
 * @param mode points to a string beginning with one of the following sequences: r (read), r+ (read + write),
 *        w (write), w+ (read + write), a (append), a+ (read + append).
 *        Can optionally include a 'b' as the last character to signify binary mode
 * @return Upon successful completion, returns a FILE pointer. Otherwise, NULL is returned.
 */
FILE *
open_file(char *pathname, const char *mode);

/**
 * @brief Reads nmemb items of data, each size bytes long, from the stream pointed to by stream, storing them at the
 *        location given by ptr.
 * @param ptr the location the data read is to be stored at
 * @param size the size of each member to be read from the stream
 * @param nmemb the number of members to be read. Reading more than the number of members available in the stream will
 *        result in undefined behavior.
 * @param stream the file stream to be read from
 * @return On success, returns the number of items read. This number equals the number of bytes transferred only when
 *         size is 1.  If an error occurs, or EOF is reached, the return value is a short item count (or zero).
 */
size_t
read_file(void *ptr, size_t size, size_t nmemb, FILE *stream);

/**
 * @brief Flushes the stream pointed to by stream (writing any buffered output data using fflush(3)) and closes the
 *        underlying file descriptor.
 * @param stream the file stream to be closed
 * @return Upon successful completion, 0 is returned. Otherwise, EOF is returned. In either case, any further access
 *         (including another call to close_file() or fclose()) to the stream results in undefined behavior.
 */
int
close_file(FILE *stream);

/**
 * @brief Writes nmemb items of data, each size bytes long, to the stream pointed to by stream, obtaining them from the
 *        location given by ptr.
 * @param ptr the location data to be written is located
 * @param size the size of each member to be written to the stream
 * @param nmemb the number of members to be written. Reading more than the number of members available in the stream will
 *        result in undefined behavior.
 * @param stream the file stream to be written to
 * @return On success, returns the number of items written. This number equals the number of bytes transferred only when
 *         size is 1.  If an error occurs, or EOF is reached, the return value is a short item count (or zero).
 */
size_t
write_file(const void *ptr, size_t size, size_t nmemb, FILE *stream);

/**
 * @brief Returns a mode_t specifying the file type and mode at pathname.
 * @param pathname the location of a file to get the attributes of
 * @return On success, zero is returned. On error, -1 is returned.
 */
mode_t
get_attributes_file(char *pathname);

/**
 * @brief Returns a mode_t specifying the file type and mode of a file descriptor.
 * @param fd fileno of an open file descriptor to get the attributes of
 * @return On success, zero is returned. On error, -1 is returned.
 */
mode_t
get_attributes_fd_file(int fd);

/**
 * @brief Changes a files mode bits given a path to a file. (The file mode consists of the file permission bits plus
 *        the set-user-ID, set-group-ID, and sticky bits.)
 * @param pathname path to a file to change the mode bits of, which is dereferenced if it is a symbolic link
 * @param mode a bit mask created by ORing together zero or more of the following:
 *        S_ISUID  (04000)  set-user-ID (set process effective user ID on execve(2))
 *        S_ISGID  (02000)  set-group-ID (set process effective group ID on execve(2); mandatory locking, as described
 *                          in fcntl(2); take a new file's group from parent directory, as described in chown(2)
 *                          and mkdir(2))
 *        S_ISVTX  (01000)  sticky bit (restricted deletion flag, as described in unlink(2))
 *        S_IRUSR  (00400)  read by owner
 *        S_IWUSR  (00200)  write by owner
 *        S_IXUSR  (00100)  execute/search by owner ("search" applies for directories, and means that entries within the
 *                          directory can be accessed)
 *        S_IRGRP  (00040)  read by group
 *        S_IWGRP  (00020)  write by group
 *        S_IXGRP  (00010)  execute/search by group
 *        S_IROTH  (00004)  read by others
 *        S_IWOTH  (00002)  write by others
 *        S_IXOTH  (00001)  execute/search by others
 * @return On success, zero is returned. On error, -1 is returned.
 */
int
change_attributes_file(const char *pathname, mode_t mode);

/**
 * @brief Changes a files mode bits given a file descriptor. (The file mode consists of the file permission bits plus
 *        the set-user-ID, set-group-ID, and sticky bits.)
 * @param fd an open file descriptor for the file to change the mode bits of
 * @param mode a bit mask created by ORing together zero or more of the following:
 *        S_ISUID  (04000)  set-user-ID (set process effective user ID on execve(2))
 *        S_ISGID  (02000)  set-group-ID (set process effective group ID on execve(2); mandatory locking, as described
 *                          in fcntl(2); take a new file's group from parent directory, as described in chown(2)
 *                          and mkdir(2))
 *        S_ISVTX  (01000)  sticky bit (restricted deletion flag, as described in unlink(2))
 *        S_IRUSR  (00400)  read by owner
 *        S_IWUSR  (00200)  write by owner
 *        S_IXUSR  (00100)  execute/search by owner ("search" applies for directories, and means that entries within the
 *                          directory can be accessed)
 *        S_IRGRP  (00040)  read by group
 *        S_IWGRP  (00020)  write by group
 *        S_IXGRP  (00010)  execute/search by group
 *        S_IROTH  (00004)  read by others
 *        S_IWOTH  (00002)  write by others
 *        S_IXOTH  (00001)  execute/search by others
 * @return On success, zero is returned. On error, -1 is returned.
 */
int
change_attributes_fd_file(int fd, mode_t mode);

/**
 * @brief Deletes a name from the filesystem. Calls remove(3) which calls unlink(2) for files,
 *        and rmdir(2) for directories.
 * @note -If the removed name was the last link to a file and no processes have the file open, the file is deleted and
 *       the space it was using is made available for reuse.
 *       -If the name was the last link to a file, but any processes still have the file open, the file will remain in
 *       existence until the last file descriptor referring to it is closed.
 *       -If the name referred to a symbolic link, the link is removed.
 *       -If the name referred to a socket, FIFO, or device, the name is removed, but processes which have the object
 *       open may continue to use it.
 * @param pathname the location of a file to delete
 * @return On success, zero is returned.  On error, -1 is returned.
 */
int
delete_file(const char *pathname);

/**
 * Returns a time_t specifying the time and date of last modification of a file's inode
 * @param pathname the location of a file to get the create date/time from
 * @return time_t containing the create date/time
 */
time_t
get_date_created_file(char *pathname);

/**
 * Returns a time_t specifying the time and date of last modification of a file's inode
 * @param fd fileno of an open file descriptor to get the create date/time from
 * @return time_t containing the create date/time
 */
time_t
get_date_created_fd_file(int fd);

/**
 * Returns a time_t specifying the time and date of last modification of a file's contents
 * @param pathname the location of a file to get the modify date/time from
 * @return time_t containing the modify date/time
 */
time_t
get_date_modified_file(char *pathname);

/**
 * Returns a time_t specifying the time and date of last modification of a file's contents
 * @param fd an open file descriptor to get the modify date/time from
 * @return time_t containing the modify date/time
 */
time_t
get_date_modified_fd_file(int fd);

#endif //FILE_LIB_H
