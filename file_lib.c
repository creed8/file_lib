//
// Created by csreed on 11/1/19.
//

#include "file_lib.h"

FILE *
open_file(char *pathname, const char *mode)
{
    FILE *fd = NULL;
    if((fd = fopen(pathname, mode)) == NULL) {
        perror("open_file");
    }
    return fd;
}

size_t
read_file(void *ptr, size_t size, size_t nmemb, FILE *stream)
{
    size_t read_amt = 0;
    if((read_amt = fread(ptr, size, nmemb, stream)) == 0) {
        fprintf(stderr, "Error encountered while reading from fileno: %d", fileno(stream));
    }
    return read_amt;
}

int
close_file(FILE *stream)
{
    int status = 0;
    if((status = fclose(stream)) == EOF) {
        perror("close_file");
    }
    return status;
}

size_t
write_file(const void *ptr, size_t size, size_t nmemb, FILE *stream)
{
    size_t write_amt = 0;
    if ((write_amt = fwrite(ptr, size, nmemb, stream)) < (size * nmemb)) {
        fprintf(stderr, "Error encountered while writing to fileno: %d", fileno(stream));
    }
    return write_amt;
}

mode_t
get_attributes_file(char *pathname)
{
    mode_t attr = 0;
    struct stat filestats;
    if ((stat(pathname, &filestats)) == -1) {
        perror("get_attributes_file");
    }
    else {
        attr = filestats.st_mode;
    }
    return attr;
}

mode_t
get_attributes_fd_file(int fd)
{
    mode_t attr = 0;
    struct stat filestats;
    if ((fstat(fd, &filestats)) == -1) {
        perror("get_attributes_file");
    }
    else {
        attr = filestats.st_mode;
    }
    return attr;
}

int
change_attributes_file(const char *pathname, mode_t mode)
{
    int status = 0;
    if ((status = chmod(pathname, mode)) == -1) {
        perror("change_attributes_file");
    }
    return status;
}

int
change_attributes_fd_file(int fd, mode_t mode)
{
    int status = 0;
    if ((status = fchmod(fd, mode)) == -1) {
        perror("change_attributes_file");
    }
    return status;
}

int
delete_file(const char *pathname)
{
    int status;
    if ((status = remove(pathname)) == -1) {
        perror("delete_file");
    }
    return status;
}

time_t
get_date_created_file(char *pathname)
{
    time_t ctime = 0;
    struct stat filestats;
    if ((stat(pathname, &filestats)) == -1) {
        perror("get_date_created_file");
    }
    else {
        ctime = filestats.st_ctim.tv_sec;
    }
    return ctime;
}

time_t
get_date_created_fd_file(int fd)
{
    time_t ctime = 0;
    struct stat filestats;
    if ((fstat(fd, &filestats)) == -1) {
        perror("get_date_created_fd_file");
    }
    else {
        ctime = filestats.st_ctim.tv_sec;
    }
    return ctime;
}

time_t
get_date_modified_file(char *pathname)
{
    time_t mtime = 0;
    struct stat filestats;
    if ((stat(pathname, &filestats)) == -1) {
        perror("get_date_modified_file");
    }
    else {
        mtime = filestats.st_mtim.tv_sec;
    }
    return mtime;
}

time_t
get_date_modified_fd_file(int fd)
{
    time_t mtime = 0;
    struct stat filestats;
    if ((fstat(fd, &filestats)) == -1) {
        perror("get_date_modified_fd_file");
    }
    else {
        mtime = filestats.st_mtim.tv_sec;
    }
    return mtime;
}